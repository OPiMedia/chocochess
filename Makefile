.SUFFIXES:

###########
# Options #
###########
MAKE      = make
MAKEFLAGS =

SHELL = sh



###########
# Targets #
###########
.PHONY:	all doc jlint junit

all:
	$(MAKE) $(MAKEFLAGS) -C src all

doc:
	$(MAKE) $(MAKEFLAGS) -C src doc

jlint:
	$(MAKE) $(MAKEFLAGS) -C src jlint

junit:
	$(MAKE) $(MAKEFLAGS) -C src junit



#########
# Clean #
#########
.PHONY:	clean distclean overclean

clean:
	$(MAKE) $(MAKEFLAGS) -C src clean

distclean:
	$(MAKE) $(MAKEFLAGS) -C src distclean

overclean:
	$(MAKE) $(MAKEFLAGS) -C src overclean
