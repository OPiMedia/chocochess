.. -*- restructuredtext -*-

==========
ChocoChess
==========
Projet *Satisfactions de Contraintes et Utilisation de l'Outil Choco solver*
pour le cours `INFO-F302 Informatique fondamentale`_ de l'ULB. (20/20)

.. _`INFO-F302 Informatique fondamentale`: http://www.ulb.ac.be/di/info-f-302/

Problèmes d'indépendance et de domination sur un échiquier généralisé
(taille quelconque et pièces supplémentaires).

|



Auteurs : Dany et |OPi|
=======================
| Dany Simone Efila — https://www.linkedin.com/in/dansy-efila-it/
|
| Olivier Pirson OPi — http://www.opimedia.be/
| olivier.pirson.opi@gmail.com
| https://bitbucket.org/OPiMedia
| https://twitter.com/OPirson

.. |OPi| image:: http://www.opimedia.be/_png/OPi.png

|



*Choco solver*
==============
* http://www.choco-solver.org/
* http://choco-tuto.readthedocs.io/en/latest/



|ChocoChess|

(extrait de l'image accompagnant `Chocolate checkmate your father this June`_)

.. _`Chocolate checkmate your father this June`: http://pressreleases.responsesource.com/news/31174/chocolate-checkmate-your-father-this-june/

.. |ChocoChess| image:: https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2020/Jun/15/286082453-2-chocochess-logo_avatar.png
