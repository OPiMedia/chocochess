\section{Question 5. Surveillance de musée}
\label{sec:musee}%
Remarquons que ce problème est similaire à un problème de minimisation du nombre de tours
nécessaires à la domination d'un échiquier.
Avec la particularité que certaines cases de l'échiquier sont initialement occupées par un obstacle
(qui n'a aucune autre action que de celle d'occuper une case),
et qu'à la place de tours proprement dite il y a quatre types de ``tours'',
chacune ne pouvant se déplacer que dans une seule direction.
Ces $5$ types de pièces ont été ajoutées à notre programme général
comme expliqué page \pageref{generalisation}.

\medskip
Soit l'instance du problème
$I = (m, n, O)$,
avec $m, n \in \naturals*$,
$M = \{1, 2, 3, \dots, m\}$,
$N = \{1, 2, 3, \dots, n\}$
et $O \subseteq M \times N$.

Il s'agit d'un musée de taille $m \times n$
qui contient un ensemble d'obstacles
\footnote{
  Des obstacles proprement dit \texttt{*}
  \begin{picture}(0,0)
    \put(0,0){\chessboard[tinyboard,label=false,pgf=true,showmover=false,maxfield=a1,pgfstyle=color,markfields={a1}], et non des capteurs.}
\end{picture}}
prédéterminé représenté par l'ensemble $O$ des couples de coordonnées.



\subsection{Formulation CSP du problème}
\subsubsection*{Variables}
L'ensemble des variables correspond aux $m n$ cases du musée :\\
$X = \{\begin{array}[t]{@{}r@{\ }r@{\ }r@{\ }r@{\ }r@{}l@{}}%
x_{1,1}, & x_{1,2}, & x_{1,3}, & \dots, & x_{1,n},\\
x_{2,1}, & x_{2,2}, & x_{2,3}, & \dots, & x_{2,n},\\
x_{3,1}, & x_{3,2}, & x_{3,3}, & \dots, & x_{3,n},\\
\dots\\
x_{m,1}, & x_{m,2}, & x_{m,3}, & \dots, & x_{m,n} & \}\end{array}$.

Une variable $x_{i,j}$
correspondant à la case de la ligne $i$ et de la colonne $j$ du musée.


\subsubsection*{Domaines}
$\forall i \in M, \forall j \in N :$ le domaine associé à la variable $x_{i,j}$
est $D_{i,j} = \{0, 1, 2, 3, 4, 5\}$.

Chacune des variables ayant le même domaine
nous le noterons $D$.

Pour chacune des cases :
\begin{itemize}
\item
  $0$ représente une case vide
\item
  $1$ une case occupée par un obstacle
\item
  $2$ par un capteur pointant vers le nord (appelé ``tour'' nord dans notre programme général)
\item
  $3$ par un capteur pointant vers le sud
\item
  $4$ par un capteur pointant vers l'est
\item
  $5$ par un capteur pointant vers l'ouest.
\end{itemize}


\subsubsection*{Contraintes}
\begin{itemize}
\item
  Contrainte assurant un nombre minimum de capteurs :

  $c_{\min} = \begin{array}[t]{@{}l@{}l@{}}%
    \big( & (x_{1,1}, x_{1,2}, x_{1,3}, \dots, x_{m,n}),\\
    & \big\{(p_{1,1}, p_{1,2}, p_{1,3}, \dots, p_{m,n}) \in D^{mn}
    \big|
    \sum\limits_{\substack{i \in M\\j \in N\\p_{i, j} \geq 2}} 1
    = \min\limits_{(p'_{1,1}, p'_{1,2}, \dots, p'_{m,n}) \in D^{mn}}
    \sum\limits_{\substack{i \in M\\j \in N\\p'_{i, j} \geq 2}} 1
    \big\}\big)
  \end{array}$
\item
  Contrainte fixant les cases occupées par un obstacle :

  $c_O = \big( (x_{1,1}, x_{1,2}, x_{1,3}, \dots, x_{m,n}),
  \big\{(p_{1,1}, p_{1,2}, p_{1,3}, \dots, p_{m,n}) \in D^{mn}
  \big|
  (i, j) \in O \leftrightarrow p_{i,j} = 1
  \big\}\big)$
\item
  Contrainte assurant que chaque case est surveillée par au moins un capteur :

  Si une case est vide
  alors soit un capteur Nord doit être sur la même colonne en-dessous sans obstacle s'interposant,
  soit un capteur Sud doit être sur la même colonne au-dessus sans obstacle s'interposant,
  soit un capteur Est doit être sur la même ligne à gauche sans obstacle s'interposant
  ou soit un capteur Ouest doit être sur la même ligne à droite sans obstacle s'interposant.

  $c_{\text{surveille}} = \begin{array}[t]{@{}l@{}l@{}}%
    \big( & (x_{1,1}, x_{1,2}, x_{1,3}, \dots, x_{m,n}),\\
    & \big\{(p_{1,1}, p_{1,2}, p_{1,3}, \dots, p_{m,n}) \in D^{mn}
    \big|\\
    & \ (p_{i,j} = 0)
    \longrightarrow
    \begin{array}[t]{r@{\ }l@{\ }l@{}}
      & \displaystyle\bigvee\limits_{i < i' \leq m}
      & (p_{i',j} = \mathbf{2})
      \conj
      \left(\displaystyle\bigwedge\limits_{i < i'' < i'} p_{i'',j} = 0\right)\\

      \disj
      & \displaystyle\bigvee\limits_{1 \leq i' < i}
      & (p_{i',j} = \mathbf{3})
      \conj
      \left(\displaystyle\bigwedge\limits_{i' < i'' < i} p_{i'',j} = 0\right)\\

      \disj
      & \displaystyle\bigvee\limits_{1 \leq j' < j}
      & (p_{i,j'} = \mathbf{4})
      \conj
      \left(\displaystyle\bigwedge\limits_{j' < j'' < j} p_{i,j''} = 0\right)\\

      \disj
      & \displaystyle\bigvee\limits_{j < j' \leq n}
      & (p_{i,j'} = \mathbf{5})
      \conj
      \left(\displaystyle\bigwedge\limits_{j < j'' < j'} p_{i,j''} = 0\right)

      \big\}\big)
    \end{array}
  \end{array}$
\end{itemize}

\bigskip
Ce qui donne l'ensemble de contraintes\\
$C_3 = \{c_{\min}\} \cup \{c_O\} \cup \{c_{\text{surveille}}\}$



\subsection{Implémentation}
Pour permettre à notre programme de traiter ce problème
nous avons dû (encore) le généraliser de deux manières.
En scindant les deux dimensions $n^2$ en $m$ et $n$ qui peuvent être différentes
(via l'option \texttt{-m n}).

Et en permettant de fixer une configuration de départ à partir d'un fichier
(via l'option \texttt{-load FILENAME},
ou à partir de l'entrée standard via l'option \texttt{-load -}).

\medskip
L'option \texttt{-museum} adapte le format d'entrée/sortie
à celui demandé pour ce problème de surveillance de musée.

\bigskip
À noter que nous avons privilégié une complète généralisation et symbiose
de tous les problèmes plutôt que de tenter d'optimiser chacun.
À noter aussi que ces problèmes sont en général intraitables sur de grandes tailles.

\newpage
%\bigskip
Exemple de la page $3$ de l'énoncé :\\
$>>>$
\begin{tabular}[t]{@{\ }l@{}}%
  \texttt{java -jar ChocoChess.jar \strong{-d -museum -minmax -tn 1 -ts 1 -te 1 -to 1 -load}}\\
  \texttt{\strong{data/museum\_7x9.txt}}
\end{tabular}\\
ou\\
$>>>$ \texttt{./museum.sh data/museum\_7x9.txt}

Configuration de départ :\\[-3ex]
\lstinputlisting{../data/museum_7x9.txt}
\begin{picture}(0,0)
\put(200,15){\chessboard[label=false,padding=-1pt,pgf=true,showmover=false,maxfield=i7,
    pgfstyle=color,markfields={a7},pgfstyle=color,markfields={b7},pgfstyle=color,markfields={c7},pgfstyle=color,markfields={d7},pgfstyle=color,markfields={e7},pgfstyle=color,markfields={f7},pgfstyle=color,markfields={g7},pgfstyle=color,markfields={h7},pgfstyle=color,markfields={i7},pgfstyle=color,markfields={a6},pgfstyle=color,markfields={c6},pgfstyle=color,markfields={d6},pgfstyle=color,markfields={e6},pgfstyle=color,markfields={i6},pgfstyle=color,markfields={a5},pgfstyle=color,markfields={i5},pgfstyle=color,markfields={a4},pgfstyle=color,markfields={c4},pgfstyle=color,markfields={d4},pgfstyle=color,markfields={f4},pgfstyle=color,markfields={g4},pgfstyle=color,markfields={i4},pgfstyle=color,markfields={a3},pgfstyle=color,markfields={c3},pgfstyle=color,markfields={d3},pgfstyle=color,markfields={f3},pgfstyle=color,markfields={g3},pgfstyle=color,markfields={i3},pgfstyle=color,markfields={a2},pgfstyle=color,markfields={f2},pgfstyle=color,markfields={g2},pgfstyle=color,markfields={i2},pgfstyle=color,markfields={a1},pgfstyle=color,markfields={b1},pgfstyle=color,markfields={c1},pgfstyle=color,markfields={d1},pgfstyle=color,markfields={e1},pgfstyle=color,markfields={f1},pgfstyle=color,markfields={g1},pgfstyle=color,markfields={h1},pgfstyle=color,markfields={i1}]}
\end{picture}

Solution :\\[-3ex]
\lstinputlisting{../data/museum_7x9_notre_solution.txt}
\begin{picture}(0,0)
\put(200,-5){\chessboard[label=false,padding=-1pt,pgf=true,showmover=false,maxfield=i7,
    pgfstyle=color,markfields={a7},pgfstyle=color,markfields={b7},pgfstyle=color,markfields={c7},pgfstyle=color,markfields={d7},pgfstyle=color,markfields={e7},pgfstyle=color,markfields={f7},pgfstyle=color,markfields={g7},pgfstyle=color,markfields={h7},pgfstyle=color,markfields={i7},pgfstyle=color,markfields={a6},pgfstyle=color,markfields={c6},pgfstyle=color,markfields={d6},pgfstyle=color,markfields={e6},pgfstyle=straightmove,markmoves={f6-g6},pgfstyle=straightmove,markmoves={h6-h5},pgfstyle=color,markfields={i6},pgfstyle=color,markfields={a5},pgfstyle=straightmove,markmoves={g5-f5},pgfstyle=color,markfields={i5},pgfstyle=color,markfields={a4},pgfstyle=color,markfields={c4},pgfstyle=color,markfields={d4},pgfstyle=color,markfields={f4},pgfstyle=color,markfields={g4},pgfstyle=color,markfields={i4},pgfstyle=color,markfields={a3},pgfstyle=color,markfields={c3},pgfstyle=color,markfields={d3},pgfstyle=straightmove,markmoves={e3-e4},pgfstyle=color,markfields={f3},pgfstyle=color,markfields={g3},pgfstyle=color,markfields={i3},pgfstyle=color,markfields={a2},pgfstyle=straightmove,markmoves={b2-b3},pgfstyle=straightmove,markmoves={c2-d2},pgfstyle=color,markfields={f2},pgfstyle=color,markfields={g2},pgfstyle=color,markfields={i2},pgfstyle=color,markfields={a1},pgfstyle=color,markfields={b1},pgfstyle=color,markfields={c1},pgfstyle=color,markfields={d1},pgfstyle=color,markfields={e1},pgfstyle=color,markfields={f1},pgfstyle=color,markfields={g1},pgfstyle=color,markfields={h1},pgfstyle=color,markfields={i1}]}
\end{picture}


\bigskip
Autre exemple,
avec la même configuration de départ mais tourné de $90\degree$ (sens antihoraire) :\\
$>>>$ \texttt{java -jar ChocoChess.jar -d -museum -minmax -tn 1 -ts 1 -te 1 -to 1 -load\\\strong{data/museum\_9x7.txt}}\\[-3ex]
\chessboard[label=false,padding=-1pt,pgf=true,showmover=false,maxfield=g9,
    pgfstyle=color,markfields={a9},pgfstyle=color,markfields={b9},pgfstyle=color,markfields={c9},pgfstyle=color,markfields={d9},pgfstyle=color,markfields={e9},pgfstyle=color,markfields={f9},pgfstyle=color,markfields={g9},pgfstyle=color,markfields={a8},pgfstyle=color,markfields={g8},pgfstyle=color,markfields={a7},pgfstyle=color,markfields={c7},pgfstyle=color,markfields={d7},pgfstyle=color,markfields={f7},pgfstyle=color,markfields={g7},pgfstyle=color,markfields={a6},pgfstyle=color,markfields={c6},pgfstyle=color,markfields={d6},pgfstyle=color,markfields={f6},pgfstyle=color,markfields={g6},pgfstyle=color,markfields={a5},pgfstyle=color,markfields={f5},pgfstyle=color,markfields={g5},pgfstyle=color,markfields={a4},pgfstyle=color,markfields={b4},pgfstyle=color,markfields={c4},pgfstyle=color,markfields={d4},pgfstyle=color,markfields={g4},pgfstyle=color,markfields={a3},pgfstyle=color,markfields={b3},pgfstyle=color,markfields={c3},pgfstyle=color,markfields={d3},pgfstyle=color,markfields={g3},pgfstyle=color,markfields={a2},pgfstyle=color,markfields={g2},pgfstyle=color,markfields={a1},pgfstyle=color,markfields={b1},pgfstyle=color,markfields={c1},pgfstyle=color,markfields={d1},pgfstyle=color,markfields={e1},pgfstyle=color,markfields={f1},pgfstyle=color,markfields={g1}]
  \chessboard[label=false,padding=-1pt,pgf=true,showmover=false,maxfield=g9,
    pgfstyle=color,markfields={a9},pgfstyle=color,markfields={b9},pgfstyle=color,markfields={c9},pgfstyle=color,markfields={d9},pgfstyle=color,markfields={e9},pgfstyle=color,markfields={f9},pgfstyle=color,markfields={g9},pgfstyle=color,markfields={a8},pgfstyle=straightmove,markmoves={b8-c8},pgfstyle=color,markfields={g8},pgfstyle=color,markfields={a7},pgfstyle=straightmove,markmoves={b7-b6},pgfstyle=color,markfields={c7},pgfstyle=color,markfields={d7},pgfstyle=color,markfields={f7},pgfstyle=color,markfields={g7},pgfstyle=color,markfields={a6},pgfstyle=color,markfields={c6},pgfstyle=color,markfields={d6},pgfstyle=color,markfields={f6},pgfstyle=color,markfields={g6},pgfstyle=color,markfields={a5},pgfstyle=straightmove,markmoves={b5-c5},pgfstyle=color,markfields={f5},pgfstyle=color,markfields={g5},pgfstyle=color,markfields={a4},pgfstyle=color,markfields={b4},pgfstyle=color,markfields={c4},pgfstyle=color,markfields={d4},pgfstyle=straightmove,markmoves={f4-f3},pgfstyle=color,markfields={g4},pgfstyle=color,markfields={a3},pgfstyle=color,markfields={b3},pgfstyle=color,markfields={c3},pgfstyle=color,markfields={d3},pgfstyle=color,markfields={g3},pgfstyle=color,markfields={a2},pgfstyle=straightmove,markmoves={b2-c2},pgfstyle=straightmove,markmoves={e2-e3},pgfstyle=color,markfields={g2},pgfstyle=color,markfields={a1},pgfstyle=color,markfields={b1},pgfstyle=color,markfields={c1},pgfstyle=color,markfields={d1},pgfstyle=color,markfields={e1},pgfstyle=color,markfields={f1},pgfstyle=color,markfields={g1}]

Remarquons que le solveur trouve une autre solution que la solution précédente tournée de $90\degree$.
