\section{Problèmes d'échecs}

Soit l'instance du problème
$I = (n, k_1, k_2, k_3)$,
avec $n \in \naturals*$ et $k_1, k_2, k_3 \in \naturals$.

Il s'agit d'un échiquier de taille $n \times n$
qui doit contenir
\begin{itemize}
\item
  $k_1$ tours
  \begin{picture}(0,0)
    \put(15,-5){\chessboard[smallboard,label=false,showmover=false,maxfield=a1,setblack={Ra1}]}
  \end{picture}
\item
  $k_2$ fous
  \begin{picture}(0,0)
    \put(0,-4){\chessboard[smallboard,label=false,showmover=false,maxfield=a1,setblack={Ba1}]}
  \end{picture}
\item
  $k_3$ cavaliers.
  \begin{picture}(0,0)
    \put(0,-4){\chessboard[smallboard,label=false,showmover=false,maxfield=a1,setblack={Na1}]}
  \end{picture}
\end{itemize}

Soit $N = \{1, 2, 3, \dots, n\}$.

\subsection{Question 1. Problème d'indépendance}
Formulation CSP (\foreignlanguage{english}{Constraint Satisfaction Problem})
du problème d'indépendance
(aucune pièce ne peut en menacer une autre).

\subsubsection*{Variables}
L'ensemble des variables correspond aux $n^2$ cases de l'échiquier :\\
$X = \{\begin{array}[t]{@{}r@{\ }r@{\ }r@{\ }r@{\ }r@{}l@{}}%
x_{1,1}, & x_{1,2}, & x_{1,3}, & \dots, & x_{1,n},\\
x_{2,1}, & x_{2,2}, & x_{2,3}, & \dots, & x_{2,n},\\
x_{3,1}, & x_{3,2}, & x_{3,3}, & \dots, & x_{3,n},\\
\dots\\
x_{n,1}, & x_{n,2}, & x_{n,3}, & \dots, & x_{n,n} & \}\end{array}$.

Une variable $x_{i,j}$
correspondant à la case de la ligne $i$ et de la colonne $j$ de l'échiquier.


\subsubsection*{Domaines}
$\forall i, j \in N :$ le domaine associé à la variable $x_{i,j}$
est $D_{i,j} = \{0, 1, 2, 3\}$.

Toutes les variables ayant le même domaine nous le noterons $D$.

Pour chacune des cases :
\begin{itemize}
\item
  $0$ représente une case vide
\item
  $1$ une case occupée par une tour
\item
  $2$ par un fou
\item
  $3$ par un cavalier.
\end{itemize}


\subsubsection*{Contraintes}
\begin{itemize}
\item
  Contrainte assurant le bon nombre de tours :

  Le nombre de $1$ sur l'échiquier vaut $k_1$.

  $c_{\text{\#tours}} = \big((x_{1,1}, x_{1,2}, x_{1,3}, \dots, x_{n,n}),
  \left\{(p_{1,1}, p_{1,2}, p_{1,3}, \dots, p_{n,n}) \in D^{n^2}
  \middle| \sum\limits_{\stackrel{i,j \in N}{p_{i,j} = \mathbf{1}}} 1 = k_1\right\}\big)$

\medskip
\item
  Contrainte assurant le bon nombre de fous :

  Le nombre de $2$ sur l'échiquier vaut $k_2$.

  $c_{\text{\#fous}} = \big((x_{1,1}, x_{1,2}, x_{1,3}, \dots, x_{n,n}),
  \left\{(p_{1,1}, p_{1,2}, p_{1,3}, \dots, p_{n,n}) \in D^{n^2}
  \middle| \sum\limits_{\stackrel{i,j \in N}{p_{i,j} = \mathbf{2}}} 1 = k_2\right\}\big)$

\medskip
\item
  Contrainte assurant le bon nombre de cavaliers :

  Le nombre de $3$ sur l'échiquier vaut $k_3$.

  $c_{\text{\#cavaliers}} = \big((x_{1,1}, x_{1,2}, x_{1,3}, \dots, x_{n,n}),
  \left\{(p_{1,1}, p_{1,2}, p_{1,3}, \dots, p_{n,n}) \in D^{n^2}
  \middle| \sum\limits_{\stackrel{i,j \in N}{p_{i,j} = \mathbf{3}}} 1 = k_3\right\}\big)$
\end{itemize}

\bigskip
Le problème d'indépendance impose que si une case est occupée par une pièce,
alors chacune des cases menacées par cette pièce doit être vide.

Remarquons que pour ce problème il n'est pas nécessaire de prendre en compte
le fait que certaines pièces puissent en bloquer d'autre.
En effet, si une pièce bloque la portée d'une autre
c'est que l'instance considérée ne remplit pas le problème d'indépendance.
Autrement dit ce problème (avec les vraies règles de mouvements du jeux d'échecs)
reste strictement équivalent si on permet à chaque pièce de traverser les autres.

\begin{itemize}
\item
  Contrainte assurant qu'aucune tour ne menace les autres pièces
  \footnote{
    Les éventuelles autres tours comprises.} :

  Si une tour occupe une case,
  les autres cases de la ligne sont vides,
  ainsi que les autres cases de la colonne.

  \begin{picture}(0,0)
    \put(-70,-40){\chessboard[tinyboard,label=false,padding=-1pt,pgf=true,showmover=false,maxfield=e5,setblack={Rc5,Ra4,Rb4,Rc4,Rd4,Re4,Rc3,Rc2,Rc1}]}
  \end{picture}
  $c_{\text{tours}} = \begin{array}[t]{@{}l@{}l@{}}%
    \big( & (x_{1,1}, x_{1,2}, x_{1,3}, \dots, x_{n,n}),\\
    & \big\{(p_{1,1}, p_{1,2}, p_{1,3}, \dots, p_{n,n}) \in D^{n^2}
    \big|
    (p_{i,j} = \mathbf{1}) \longrightarrow
    \left(\displaystyle\bigwedge\limits_{\stackrel{j' \in N}{j'\neq j}} p_{i,j'} = 0\right)
    \conj
    \left(\displaystyle\bigwedge\limits_{\stackrel{i' \in N}{i'\neq i}} p_{i',j} = 0\right)
    \big\}\big)
  \end{array}$

\medskip
\item
  Contrainte assurant qu'aucun fou ne menace les autres pièces :

  Si un fou occupe une case,
  les autres cases des deux diagonales passant par cette case sont vides.

  \begin{picture}(0,0)
    \put(-70,-40){\chessboard[tinyboard,label=false,padding=-1pt,pgf=true,showmover=false,maxfield=e5, setblack={Bb5,Bd5,Bc4,Bb3,Bd3,Ba2,Be2,Bf1}]}
  \end{picture}
  $c_{\text{fous}} = \begin{array}[t]{@{}l@{}l@{}}%
    \big( & (x_{1,1}, x_{1,2}, x_{1,3}, \dots, x_{n,n}),\\
    & \big\{(p_{1,1}, p_{1,2}, p_{1,3}, \dots, p_{n,n}) \in D^{n^2}
    \big|\\
    & \ (p_{i,j} = \mathbf{2}) \longrightarrow
    \left(\displaystyle\bigwedge\limits_{\substack{k \in -N \cup N\\1 \leq i+k \leq n\\1 \leq j+k \leq n}} p_{i+k,j+k} = 0\right)
    \conj
    \left(\displaystyle\bigwedge\limits_{\substack{k \in -N \cup N\\1 \leq i+k \leq n\\1 \leq j-k \leq n}} p_{i+k,j-k} = 0\right)
    \big\}\big)
  \end{array}$

\medskip
\item
  Contrainte assurant qu'aucun cavalier ne menace les autres pièces :

  Si un cavalier occupe une case,
  les $8$ cases possibles pour un mouvement de cavaliers sont vides.

  \begin{picture}(0,0)
    \put(-70,-40){\chessboard[tinyboard,label=false,padding=-1pt,pgf=true,showmover=false,maxfield=e5, setblack={Nb5,Nd5,Na4,Ne4,Nc3,Na2,Ne2,Nb1,Nd1}]}
  \end{picture}
  $c_{\text{cavaliers}} = \begin{array}[t]{@{}l@{}l@{}}%
    \big( & (x_{1,1}, x_{1,2}, x_{1,3}, \dots, x_{n,n}),\\
    & \big\{(p_{1,1}, p_{1,2}, p_{1,3}, \dots, p_{n,n}) \in D^{n^2}
    \big|\\
    & \ (p_{i,j} = \mathbf{3}) \longrightarrow
    \left(\displaystyle\bigwedge\limits_{\substack{k \in \{-1,1\}\\l \in \{-2,2\}\\1 \leq i+k \leq n\\1 \leq j+l \leq n}} p_{i+k,j+l} = 0\right)
    \conj
    \left(\displaystyle\bigwedge\limits_{\substack{k \in \{-1,1\}\\l \in \{-2,2\}\\1 \leq i+l \leq n\\1 \leq j+k \leq n}} p_{i+l,j+k} = 0\right)
    \big\}\big)
  \end{array}$
\end{itemize}

\bigskip
Ce qui donne l'ensemble de contraintes\\
$C_1 = \{c_{\text{\#tours}}\} \cup \{c_{\text{\#fous}}\} \cup \{c_{\text{\#cavaliers}}\}
\cup \{c_{\text{tours}}\} \cup \{c_{\text{fous}}\} \cup \{c_{\text{cavaliers}}\}$
