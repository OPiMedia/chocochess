/**
 * DominatedPositions.java (May 24, 2017)
 *
 * GPLv3 --- Copyright (C) 2017
 *         - Dany Simone Efila,
 *         - Olivier Pirson --- http://www.opimedia.be/
 *
 * @author Dany Simone Efila
 * @author Olivier Pirson
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.chocosolver.solver.Model;
import org.chocosolver.solver.constraints.Constraint;
import org.chocosolver.solver.variables.IntVar;



/**
 * Collection of DominatedPosition
 * for a piece in position (posI, posJ).
 */
public class DominatedPositions {
    final int m;
    final int n;

    final int posI;
    final int posJ;

    final List<DominatedPosition> dominatedPositions = new ArrayList<DominatedPosition>();



    public DominatedPositions(int m, int n, int posI, int posJ) {
        assert(0 <= posI);
        assert(posI < m);

        assert(0 <= posJ);
        assert(posJ < n);

        this.m = m;
        this.n = n;
        this.posI = posI;
        this.posJ = posJ;
    }



    /**
     * Add dominated positions for the piece num
     * with its positions that it must be empty.
     */
    public void add(int num) {
        switch (num) {
        case 1:
            addRook();  // tour

            break;

        case 2:
            addBishop();  // fou

            break;

        case 3:
            addKnight();  // cavalier

            break;

        case 4:
            addQueen();  // reine

            break;

        case 5:
            addKing();  // roi

            break;

        case 6:
            // obstacle: dominate nothing

            break;

        case 7:
            addNorthRook();  // "tour" nord

            break;

        case 8:
            addSouthRook();  // "tour" sud

            break;

        case 9:
            addEastRook();  // "tour" est

            break;

        case 10:
            addWestRook();  // "tour" ouest

            break;

        default:
            assert(false);
        }


        boolean assertsEnabled = false;
        assert(assertsEnabled = true);  // intentional side effect!

        if (!assertsEnabled) {
            for (DominatedPosition dominatedPosition : dominatedPositions) {
                assert(0 <= dominatedPosition.getPosI());
                assert(dominatedPosition.getPosI() < m);

                assert(0 <= dominatedPosition.getPosJ());
                assert(dominatedPosition.getPosJ() < n);

                for (Position position : dominatedPosition.positionsMustBeEmpty) {
                    assert(0 <= position.getPosI());
                    assert(position.getPosI() < m);

                    assert(0 <= position.getPosJ());
                    assert(position.getPosJ() < n);
                }
            }
        }
    }


    /**
     * Add dominated positions for bishop (fou)
     * with its positions that it must be empty.
     */
    public void addBishop() {
        int maxMN = Math.max(m, n);

        for (int offset = -(maxMN - 1); offset < maxMN; ++offset) {
            if (offset == 0) { continue; }

            final int i = posI + offset;

            if (isValidPosI(i)) {
                {
                    final int j = posJ + offset;

                    if (isValidPosJ(j)) {
                        final DominatedPosition dominatedPosition = new DominatedPosition(i, j);
                        final int sign = Integer.signum(offset);

                        for (int between = 1; between < Math.abs(offset); ++between) {
                            dominatedPosition.addPositionsMustBeEmpty
                                (new Position(posI + sign*between,
                                              posJ + sign*between));
                        }

                        dominatedPositions.add(dominatedPosition);
                    }
                }

                {
                    final int j = posJ - offset;

                    if (isValidPosJ(j)) {
                        final DominatedPosition dominatedPosition = new DominatedPosition(i, j);
                        final int sign = Integer.signum(offset);

                        for (int between = 1; between < Math.abs(offset); ++between) {
                            dominatedPosition.addPositionsMustBeEmpty
                                (new Position(posI + sign*between,
                                              posJ - sign*between));
                        }

                        dominatedPositions.add(dominatedPosition);
                    }
                }
            }
        }
    }


    /**
     * Add dominated positions for East-"rook" ("tour" est)
     * with its positions that it must be empty.
     */
    public void addEastRook() {
        for (int j = posJ + 1; j < n; ++j) {
            final DominatedPosition dominatedPosition = new DominatedPosition(posI, j);

            for (int between = posJ + 1; between < j; ++between) {
                dominatedPosition.addPositionsMustBeEmpty(new Position(posI,
                                                                       between));
            }

            dominatedPositions.add(dominatedPosition);
        }
    }


    /**
     * Add dominated positions for king (roi)
     * with its positions that it must be empty (in fact none).
     */
    public void addKing() {
        for (int offsetI = -1; offsetI <= 1; ++offsetI) {
            for (int offsetJ = -1; offsetJ <= 1; ++offsetJ) {
                if ((offsetI == 0) && (offsetJ == 0)) { continue; }

                final int i = posI + offsetI;
                final int j = posJ + offsetJ;

                if (isValidPosI(i) && isValidPosJ(j)) {
                    dominatedPositions.add(new DominatedPosition(i, j));
                }
            }
        }
    }


    /**
     * Add dominated positions for knight (cavalier)
     * with its positions that it must be empty (in fact none).
     */
    public void addKnight() {
        final int[] ones = {-1, 1};
        final int[] twos = {-2, 2};

        for (int offset1 : ones) {
            for (int offset2 : twos) {
                {
                    final int i = posI + offset1;
                    final int j = posJ + offset2;

                    if (isValidPosI(i) && isValidPosJ(j)) {
                        dominatedPositions.add(new DominatedPosition(i, j));
                    }
                }

                {
                    final int i = posI + offset2;
                    final int j = posJ + offset1;

                    if (isValidPosI(i) && isValidPosJ(j)) {
                        dominatedPositions.add(new DominatedPosition(i, j));
                    }
                }
            }
        }
    }


    /**
     * Add dominated positions for North-"rook" ("tour" nord)
     * with its positions that it must be empty.
     */
    public void addNorthRook() {
        for (int i = 0; i < posI; ++i) {
            final DominatedPosition dominatedPosition = new DominatedPosition(i, posJ);

            for (int between = i + 1; between < posI; ++between) {
                dominatedPosition.addPositionsMustBeEmpty(new Position(between,
                                                                       posJ));
            }

            dominatedPositions.add(dominatedPosition);
        }
    }


    /**
     * Add dominated positions for queen (reine)
     * with its positions that it must be empty.
     */
    public void addQueen() {
        addBishop();
        addRook();
    }


    /**
     * Add dominated positions for rook (tour)
     * with its positions that it must be empty.
     */
    public void addRook() {
        for (int offset = -(n - 1); offset < n; ++offset) {
            if (offset != 0) {
                final int j = posJ + offset;

                if (isValidPosJ(j)) {
                    final DominatedPosition dominatedPosition = new DominatedPosition(posI, j);
                    final int sign = Integer.signum(offset);

                    for (int between = 1; between < Math.abs(offset); ++between) {
                        dominatedPosition.addPositionsMustBeEmpty(new Position(posI,
                                                                               posJ + sign*between));
                    }

                    dominatedPositions.add(dominatedPosition);
                }
            }
        }

        for (int offset = -(m - 1); offset < m; ++offset) {
            if (offset != 0) {
                final int i = posI + offset;

                if (isValidPosI(i)) {
                    final DominatedPosition dominatedPosition = new DominatedPosition(i, posJ);
                    final int sign = Integer.signum(offset);

                    for (int between = 1; between < Math.abs(offset); ++between) {
                        dominatedPosition.addPositionsMustBeEmpty(new Position(posI + sign*between,
                                                                               posJ));
                    }

                    dominatedPositions.add(dominatedPosition);
                }
            }
        }

        assert(dominatedPositions.size() >= (m - 1) + (n - 1));
    }


    /**
     * Add dominated positions for South-"rook" ("tour" sud)
     * with its positions that it must be empty.
     */
    public void addSouthRook() {
        for (int i = posI + 1; i < m; ++i) {
            final DominatedPosition dominatedPosition = new DominatedPosition(i, posJ);

            for (int between = posI + 1; between < i; ++between) {
                dominatedPosition.addPositionsMustBeEmpty(new Position(between,
                                                                       posJ));
            }

            dominatedPositions.add(dominatedPosition);
        }
    }


    /**
     * Add dominated positions for West-"rook" ("tour" ouest)
     * with its positions that it must be empty.
     */
    public void addWestRook() {
        for (int j = 0; j < posJ; ++j) {
            final DominatedPosition dominatedPosition = new DominatedPosition(posI, j);

            for (int between = j + 1; between < posJ; ++between) {
                dominatedPosition.addPositionsMustBeEmpty(new Position(posI,
                                                                       between));
            }

            dominatedPositions.add(dominatedPosition);
        }
    }


    /**
     * A constraint that express that
     * for a piece in the position (posI, posJ)
     * there exists a move that it (really) dominates the position (i, j).
     */
    public Constraint dominationConstraint(Model model, IntVar[][] boxes,
                                           int piece, int i, int j) {
        assert(1 <= piece);
        assert(piece <= ChocoChess.NB_TYPE_PIECE);

        assert(0 <= i);
        assert(i < m);

        assert(0 <= j);
        assert(j < n);

        assert((i != posI) || (j != posJ));

        assert(dominatedPositions.size() > 0);

        final List<Constraint> andConstraints = new ArrayList<Constraint>();

        andConstraints.add(model.arithm(boxes[posI][posJ], "=", piece));  // piece position

        boolean isDominated = false;

        for (DominatedPosition dominatedPosition : dominatedPositions) {
            if ((dominatedPosition.getPosI() == i) && (dominatedPosition.getPosJ() == j)) {
                // piece potentially dominates position (i, j)
                assert(!isDominated);
                isDominated = true;

                // Add empty boxes constraint between position and dominated position
                for (Position emptyPosition : dominatedPosition.getPositionsMustBeEmpty()) {
                    final int emptyI = emptyPosition.getPosI();
                    final int emptyJ = emptyPosition.getPosJ();

                    assert(0 <= emptyI);
                    assert(emptyI < m);

                    assert(0 <= emptyJ);
                    assert(emptyJ < n);

                    assert((emptyI != i) || (emptyJ != j));

                    andConstraints.add(model.arithm(boxes[emptyI][emptyJ], "=", EnumPiece.EMPTY.ordinal()));
                }
            }
        }

        if (isDominated) {
            final Constraint[] andCs = andConstraints.toArray(new Constraint[andConstraints.size()]);

            final Constraint and = (andCs.length <= 1
                                    ? andCs[0]
                                    : model.and(andCs));

            return and;
        }
        else {
            return null;
        }
    }


    /**
     * A constraint that express that
     * all dominated positions must be empty.
     */
    public Constraint independenceConstraint(Model model, IntVar[][] boxes) {
        assert(dominatedPositions.size() > 0);

        final Constraint[] constraints = new Constraint[dominatedPositions.size()];

        int num = 0;

        for (DominatedPosition dominatedPosition : dominatedPositions) {
            final int i = dominatedPosition.getPosI();
            final int j = dominatedPosition.getPosJ();

            constraints[num] = model.arithm(boxes[i][j], "=", EnumPiece.EMPTY.ordinal());
            ++num;
        }

        return (dominatedPositions.size() <= 1
                ? constraints[0]
                : model.and(constraints));
    }


    /**
     * Return true iff there exists move of the position (posI, posJ)
     * that dominates the position (i, j).
     *
     * Must have (i != posI) || (j != posJ).
     */
    public boolean isDominated(Chessboard chessboard, int i, int j) {
        assert(m == chessboard.getM());
        assert(n == chessboard.getN());

        assert(0 <= i);
        assert(i < m);

        assert(0 <= j);
        assert(j < n);

        assert((i != posI) || (j != posJ));

        for (DominatedPosition dominatedPosition : dominatedPositions) {
            if ((dominatedPosition.getPosI() == i) && (dominatedPosition.getPosJ() == j)) {
                // dominatedPosition potentially dominates (i, j)
                boolean isEmpty = true;

                for (Position position : dominatedPosition.getPositionsMustBeEmpty()) {
                    if (chessboard.get(dominatedPosition.getPosI(), dominatedPosition.getPosJ())
                        != EnumPiece.EMPTY.ordinal()) {
                        isEmpty = false;

                        break;
                    }
                }

                if (isEmpty) {  // there is no other piece between dominatedPosition and (i, j)
                    return true;
                }
            }
        }

        return false;
    }


    public boolean isEmpty() {
        return (dominatedPositions.size() == 0);
    }


    /**
     * Return true iff all dominated positions by the position (posI, posJ) are empty.
     */
    public boolean isIndependent(Chessboard chessboard) {
        assert(m == chessboard.getM());
        assert(n == chessboard.getN());

        for (DominatedPosition dominatedPosition : dominatedPositions) {
            if (chessboard.get(dominatedPosition.getPosI(), dominatedPosition.getPosJ())
                != EnumPiece.EMPTY.ordinal()) {
                return false;
            }
        }

        return true;
    }


    public boolean isValidPosI(int coordonate) {
        return (0 <= coordonate) && (coordonate < m);
    }


    public boolean isValidPosJ(int coordonate) {
        return (0 <= coordonate) && (coordonate < n);
    }


    public void printAllPositionsMustBeEmpty(int symbol, boolean isMuseumDisplay) {
        for (DominatedPosition dominatedPosition: dominatedPositions) {
            System.err.println("---");
            System.err.println(dominatedPosition.toString());

            final int[][] boxes = new int[m][n];

            boxes[posI][posJ] = symbol;

            assert((posI != dominatedPosition.getPosI())
                   || (posJ != dominatedPosition.getPosJ()));

            boxes[dominatedPosition.getPosI()][dominatedPosition.getPosJ()]
                = EnumPiece.DOMINATED.ordinal();

            for (Position positionMustBeEmpty: dominatedPosition.getPositionsMustBeEmpty()) {
                final int i = positionMustBeEmpty.getPosI();
                final int j = positionMustBeEmpty.getPosJ();

                assert(boxes[i][j] == EnumPiece.EMPTY.ordinal());
                boxes[i][j] = EnumPiece.MUSTBEEMPTY.ordinal();
            }

            final Chessboard chessboard = new Chessboard(boxes);

            System.err.println(chessboard.toString(isMuseumDisplay));
        }
    }


    public String toString(char positionSymbol, boolean isMuseumDisplay) {
        final String[][] stringChessboard = new String[m][n];

        // Set each box as empty
        for (int i = 0; i < m; ++i) {
            Arrays.fill(stringChessboard[i], EnumPiece.EMPTY.toString(isMuseumDisplay));
        }

        // Set current position
        stringChessboard[posI][posJ] = String.valueOf(positionSymbol);

        // Set all dominated position
        for (DominatedPosition position : dominatedPositions) {
            final int i = position.getPosI();
            final int j = position.getPosJ();

            assert(stringChessboard[i][j].equals(EnumPiece.EMPTY.toString(isMuseumDisplay)));

            stringChessboard[i][j] = EnumPiece.DOMINATED.toString(isMuseumDisplay);
        }


        // Build lines
        final String[] lines = new String[m];

        for (int i = 0; i < m; ++i) {
            lines[i] = String.join(" ", stringChessboard[i]);
        }

        return String.join("\n", lines);
    }
}
