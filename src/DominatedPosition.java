/**
 * DominatedPosition.java (May 25, 2017)
 *
 * GPLv3 --- Copyright (C) 2017
 *         - Dany Simone Efila,
 *         - Olivier Pirson --- http://www.opimedia.be/
 *
 * @author Dany Simone Efila
 * @author Olivier Pirson
 */

import java.util.ArrayList;
import java.util.List;



/**
 * Represents a domininated position
 * with a collection of all other positions that must be empty
 * so that this position is effectively dominated.
 */
public class DominatedPosition {
    final Position dominatedPosition;

    final List<Position> positionsMustBeEmpty = new ArrayList<Position>();


    public DominatedPosition(int posI, int posJ) {
        assert(0 <= posI);
        assert(0 <= posJ);

        dominatedPosition = new Position(posI, posJ);
    }


    public void addPositionsMustBeEmpty(Position position) {
        positionsMustBeEmpty.add(position);
    }


    public int getPosI() { return dominatedPosition.getPosI(); }

    public int getPosJ() { return dominatedPosition.getPosJ(); }

    public List<Position> getPositionsMustBeEmpty() { return positionsMustBeEmpty; }


    @Override
    public String toString() {
        return "(" + getPosI() + ", " + getPosJ() + "): " + positionsMustBeEmpty.size();
    }
}
