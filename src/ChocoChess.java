/**
 * ChocoChess.java (May 26, 2017)
 *
 * INFO-F302 Informatique fondamentale
 * Projet Satisfactions de Contraintes et Utilisation de l'Outil Choco solver
 *
 * Problème d'indépendance et problème de domination
 *
 * GPLv3 --- Copyright (C) 2017
 *         - Dany Simone Efila,
 *         - Olivier Pirson --- http://www.opimedia.be/
 *
 * @author Dany Simone Efila
 * @author Olivier Pirson
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solution;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.Constraint;
import org.chocosolver.solver.variables.IntVar;



/**
 * Main class
 */
public class ChocoChess {
    static final int NB_TYPE_PIECE = 3 + 2 + 1 + 4;


    /** Initial configuration */
    Chessboard initialChessboard = null;

    /** Number of the determined pieces in the initial configuration */
    int[] initialNumberPieces = {0,
                                 0, 0, 0,
                                 0, 0,
                                 0,
                                 0, 0, 0, 0};

    /** If find all solutions instead one solution */
    boolean isAll = false;

    /** If display dominated positions of each solution */
    boolean isDebugPos = false;

    /** Independence problem or domination problem */
    boolean isIndependenceProblem = true;

    /** If display also solution(s) in LaTeX format for the chessboard package */
    boolean isLatex = false;

    /** Minimize/maximize number of not null pieces */
    boolean isMinMax = false;

    /** Museum format to display solution */
    boolean isMuseumDisplay = false;

    /** If symmetrics optimizations is enabled */
    boolean isSymmetricsOptimizations = false;

    /* If display some informations to stderr */
    boolean isVerbose = false;

    /** Number of lines (-1 to represent not specified value, then will be equal to n) */
    int m = -1;

    /** Number of columns */
    int n = 8;

    String[] nameFrenchPieces = {"vide",
                                 "tour", "fou", "cavalier",
                                 "reine", "roi",
                                 "obstacle",
                                 "nord", "sud", "est", "ouest"};

    String[] namePieces = {"empty",
                           "rook", "bishop", "knight",
                           "queen", "king",
                           "obstacle",
                           "North", "South", "East", "West"};

    /** Number of the determined pieces (in addition to the initial configuration) */
    int[] numberPieces = {64,
                          0, 0, 0,
                          0, 0,
                          0,
                          0, 0, 0, 0};



    public ChocoChess() {
        assert(nameFrenchPieces.length == NB_TYPE_PIECE + 1);
        assert(namePieces.length == NB_TYPE_PIECE + 1);

        assert(numberPieces.length == NB_TYPE_PIECE + 1);
        assert(initialNumberPieces.length == NB_TYPE_PIECE + 1);
    }



    public void help(int errorCode) {
        System.err.println("Usage: java -jar ChocoChess.jar [options]");
        System.err.println("  -[i|d]: Independence problem (by default) or Domination problem");
        System.err.println("  -m n: number of chessboard lines (same of columns by default)");
        System.err.println("  -n n: number of chessboard column (8 by default)");
        System.err.println("  -minmax: minimize number of not null pieces for Independence problem");
        System.err.println("           / maximize number of not null pieces for Domination problem");
        System.err.println("           (instead find exact number of pieces)");
        System.err.println();
        System.err.println("  -t n: number of rooks   (Tours)     (0 by default)");
        System.err.println("  -f n: number of bishops (Fous)      (0 by default)");
        System.err.println("  -c n: number of knights (Cavaliers) (0 by default)");
        System.err.println("  -r n: number of queens  (Reines)    (0 by default)");
        System.err.println("  -k n: number of Kings   (rois)      (0 by default)");
        System.err.println();
        System.err.println("  -o n: number of obstacles (0 by default)");
        System.err.println("  -tn n: number of North-\"rooks\" (\"Tours\" Nord)  (0 by default)");
        System.err.println("  -ts n: number of South-\"rooks\" (\"Tours\" Sud)   (0 by default)");
        System.err.println("  -te n: number of East-\"rooks\"  (\"Tours\" Est)   (0 by default)");
        System.err.println("  -to n: number of West-\"rooks\"  (\"Tours\" Ouest) (0 by default)");
        System.err.println();
        System.err.println("  -load FILENAME: load initial configuration in chess or museum format");
        System.err.println("  -load -: load initial configuration from standard input stream");
        System.err.println("  -museum: museum format to load file and display solution(s)");
        System.err.println();
        System.err.println("  -all: find all solutions instead one solution (only if not -minmax)");
        System.err.println("  -help: display this message to standard error stream and quit");
        System.err.println("  -latex: display also solution(s) in LaTeX format for the chessboard package");
        System.err.println("  -symmetrics-optimizations: enable some symmetrics optimizations");
        System.err.println("                             (requires m = n and no initial configuration)");
        System.err.println("  -verbose: display some informations to standard error stream");
        System.err.println();
        System.err.println("  -debug-pos: display dominated positions to standard error stream");

        System.exit(errorCode);
    }


    public void modelAndSolve() {
        final Model model = new Model("Chess Independence/Domination Problem " + m + " x " + n);


        // Variables
        ////////////
        final IntVar[][] boxes = new IntVar[m][n];  // each box of the chessboard

        // Set each box
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                // Not determinated or determinated piece on this box
                boxes[i][j] = (initialChessboard.get(i, j) == EnumPiece.EMPTY.ordinal()
                               ? model.intVar("case_" + i + ',' + j, 0, NB_TYPE_PIECE)
                               : model.intVar("case_" + i + ',' + j, initialChessboard.get(i, j)));
            }
        }

        final IntVar[] countPieces = new IntVar[NB_TYPE_PIECE + 1];  // empty or each piece

        // Set count of each piece
        for (int num = 0; num < countPieces.length; ++num) {
            if (isMinMax) {  // For minimization/maximization problem
                if (num == EnumPiece.EMPTY.ordinal()) {  // possibly empty boxes
                    countPieces[num] = model.intVar("count_piece_" + num,
                                                    0, initialNumberPieces[num]);
                }
                else if (numberPieces[num] == 0) {       // not available piece
                    countPieces[num] = model.intVar("count_piece_" + num,
                                                    initialNumberPieces[num]);
                }
                else {                                   // available piece
                    countPieces[num] = model.intVar("count_piece_" + num,
                                                    initialNumberPieces[num], m*n);
                }
            }
            else {           // Exact required number of this piece
                countPieces[num] = model.intVar("count_piece_" + num,
                                                initialNumberPieces[num] + numberPieces[num]);
            }
        }


        // Set subsets of variables
        final int halfM = m/2 + m % 2;
        final int halfN = n/2 + n % 2;
        final IntVar[] allSquares = new IntVar[m*n];
        final IntVar[] leftTopQuarter = new IntVar[halfM*halfN];

        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                allSquares[i*n + j] = boxes[i][j];

                if ((i < halfM) && (j < halfN)) {
                    leftTopQuarter[i*halfN + j] = boxes[i][j];
                }
            }
        }


        // Constraints
        //////////////
        // Additional constraint
        if (isSymmetricsOptimizations) {
            assert(m == n);
            assert(initialNumberPieces[EnumPiece.EMPTY.ordinal()] == m*n);  // no initial config

            for (int num = 1; num < numberPieces.length; ++num) {
                if (numberPieces[num] > 0) {
                    // First piece required in the left-top quarter
                    model.count(num, leftTopQuarter, model.intVar(1, leftTopQuarter.length)).post();

                    break;
                }
            }
        }

        // Constraints
        for (int num = 0; num < numberPieces.length; ++num) {
            model.count(num, allSquares, countPieces[num]).post();  // number of the piece num
        }
        model.sum(countPieces, "=", m*n);  // sum of all countPieces

        if (isIndependenceProblem) {
            setConstraintsIndependence(model, boxes);
        }
        else {
            setConstraintsDomination(model, boxes);
        }

        if (isMinMax) {  // minimization/maximization problem
            // Independence problem: Minimize number of empty boxes,
            //                       to maximize number of required pieces
            // Domination problem: Maximize number of empty boxes,
            //                     to minimize number of required pieces
            model.setObjective((isIndependenceProblem
                                ? Model.MINIMIZE
                                : Model.MAXIMIZE), countPieces[0]);
        }

        System.out.flush();
        System.err.flush();


        // Solve
        ////////
        final Solver solver = model.getSolver();

        List<Solution> solutions = new ArrayList<>();

        if (isMinMax) {
            final Solution solution = new Solution(model);
            boolean founded = false;

            while (solver.solve()) {
                solution.record();
                if (isVerbose) {
                    System.err.println("Found with total # of pieces: "
                                       + (m*n - countPieces[0].getValue()));

                    final Chessboard chessboard = new Chessboard(boxes, solution);

                    System.err.println(m*n
                                       - solution.getIntVal(countPieces[EnumPiece.EMPTY.ordinal()])
                                       - solution.getIntVal(countPieces[EnumPiece.OBSTACLE.ordinal()]));
                    System.err.println(chessboard.toString(isMuseumDisplay));
                    System.err.flush();
                    if ((isIndependenceProblem && !chessboard.isIndependent())
                        || (!isIndependenceProblem && !chessboard.isDominated())) {
                        System.err.println("Incorrect solution: NOT "
                                           + (isIndependenceProblem
                                              ? "independent"
                                              : "dominated") + '!');
                    }
                }
                founded = true;
            }

            if (founded) {
                solutions.add(solution);
            }
        }
        else if (isAll) {
            solutions = solver.findAllSolutions();
        }
        else {
            final Solution solution = solver.findSolution();

            if (solution != null) {
                solutions.add(solution);
            }
        }

        if (isVerbose) {
            System.err.println(solver.toMultiLineString());
        }


        // Results
        //////////
        if (solutions.size() == 0) {
            System.out.println("pas de solution");
        }
        else {
            int solutionNum = 0;

            for (Solution solution : solutions) {
                if (isAll) {
                    ++solutionNum;
                    System.out.println("===== Solution n°" + solutionNum
                                       + '/' + solutions.size() + " =====");
                }

                if (isVerbose) {
                    System.err.print("# pieces:");
                    for (int num = 0; num < numberPieces.length; ++num) {
                        System.err.print("\t" + solution.getIntVal(countPieces[num]));
                    }
                    System.err.println();
                }

                if (isMinMax) {
                    System.out.println(m*n
                                       - solution.getIntVal(countPieces[EnumPiece.EMPTY.ordinal()])
                                       - solution.getIntVal(countPieces[EnumPiece.OBSTACLE.ordinal()]));
                }


                final Chessboard chessboard = new Chessboard(boxes, solution);


                // Display this solution
                System.out.println(chessboard.toString(isMuseumDisplay));
                if (isLatex) {
                    System.out.println(chessboard.toStringLatex());
                }


                if (isDebugPos) {
                    // Display dominated positions by each piece of the solution
                    chessboard.printAllDominatedPositions(isMuseumDisplay);
                }


                // Check this solution (Java code to verify solver use)
                if ((isIndependenceProblem && !chessboard.isIndependent())
                    || (!isIndependenceProblem && !chessboard.isDominated())) {
                    System.out.println("Incorrect solution: NOT "
                                       + (isIndependenceProblem
                                          ? "independent"
                                          : "dominated") + '!');

                    System.exit(1);
                }
            }
        }
    }


    public void printInfos() {
        System.err.println("Chess " + (isIndependenceProblem
                                       ? "Independence"
                                       : "Domination") + " problem");
        System.err.println((isIndependenceProblem
                            ? "Maximization "
                            : "Minimization ") + ": " + isMinMax);
        System.err.println("Museum display: " + isMuseumDisplay);
        System.err.println("m x n: " + m + " x " + n + " = " + (m*n));

        for (int num = 0; num < numberPieces.length; ++num) {
            final String s = "# " + namePieces[num] + " (" + nameFrenchPieces[num] + "):"
                + (new String(new char[22]).replace("\0", " "));

            System.err.println(s.substring(0, 22)
                               + '\t' + initialNumberPieces[num] + "\t+ " + numberPieces[num]
                               + "\t= " + (initialNumberPieces[num] + numberPieces[num]));
        }

        if (!isMinMax) {
            System.err.println("Find all solutions: " + isAll);
        }
        System.err.println("Symmetrics optimizations: " + isSymmetricsOptimizations);

        if (initialNumberPieces[EnumPiece.EMPTY.ordinal()] < m*n) {
            System.err.println("Initial configuration: "
                               + ((isIndependenceProblem && !initialChessboard.isIndependent())
                                  || (!isIndependenceProblem && !initialChessboard.isDominated())
                                  ? "NOT "
                                  : "")
                               + (isIndependenceProblem
                                  ? "independent"
                                  : "dominated"));
            System.err.println(initialChessboard.toString(isMuseumDisplay));
        }
    }


    /**
     * Set constraint that express that
     * each empty position must be dominated by at least one piece.
     */
    public void setConstraintsDomination(Model model, IntVar[][] boxes) {
        assert(EnumPiece.EMPTY.ordinal() == 0);

        // For each position (i, j)
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                final List<Constraint> constraints = new ArrayList<Constraint>();

                // For each position (posI, posJ) != (i, j)
                for (int posI = 0; posI < m; ++posI) {
                    for (int posJ = 0; posJ < n; ++posJ) {
                        if ((posI != i) || (posJ != j)) {
                            // For each type of piece at the position (posI, posJ)
                            for (int num = 1; num < numberPieces.length; ++num) {
                                if (initialNumberPieces[num] + numberPieces[num] > 0) {
                                    final DominatedPositions dominatedPositions
                                        = new DominatedPositions(m, n, posI, posJ);

                                    dominatedPositions.add(num);

                                    if (!dominatedPositions.isEmpty()) {
                                        final Constraint c
                                            = dominatedPositions.dominationConstraint(model, boxes,
                                                                                      num, i, j);

                                        if (c != null) {
                                            constraints.add(c);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (constraints.size() > 0) {
                    // There exists a move that can dominate the position (i, j)
                    final Constraint[] cs = constraints.toArray(new Constraint[constraints.size()]);
                    final Constraint or = (cs.length <= 1
                                           ? cs[0]
                                           : model.or(cs));

                    // If the position (i, j) is empty
                    // then a move must be dominate the position (i, j)
                    model.ifThen(model.arithm(boxes[i][j], "=", EnumPiece.EMPTY.ordinal()),
                                 model.or(or));
                }
                else {
                    // There don't exists a move that can dominate the position (i, j)
                    // so it must be not empty
                    model.arithm(boxes[i][j], "!=", EnumPiece.EMPTY.ordinal()).post();
                }
            }
        }
    }


    /**
     * Set constraint that express that
     * each piece dominates only empty positions.
     */
    public void setConstraintsIndependence(Model model, IntVar[][] boxes) {
        assert(EnumPiece.EMPTY.ordinal() == 0);

        // For each available type of piece
        for (int num = 1; num < numberPieces.length; ++num) {
            if (initialNumberPieces[num] + numberPieces[num] > 0) {
                // For each position
                for (int i = 0; i < m; ++i) {
                    for (int j = 0; j < n; ++j) {
                        final DominatedPositions dominatedPositions
                            = new DominatedPositions(m, n, i, j);

                        dominatedPositions.add(num);

                        if (!dominatedPositions.isEmpty()) {
                            // If the position (i, j) contains a piece num
                            // then all its dominated positions must be empty
                            model.ifThen(model.arithm(boxes[i][j], "=", num),
                                         dominatedPositions.independenceConstraint(model, boxes));
                        }
                    }
                }
            }
        }
    }


    public void setParams(String[] args) {
        for (int i = 0; i < args.length; ++i) {
            final String arg = args[i];

            final String[] pieceParams = {null,
                                          "-t", "-f", "-c",
                                          "-r", "-k",
                                          "-o",
                                          "-tn", "-ts", "-te", "-to"};
            final int indexParam = Arrays.asList(pieceParams).indexOf(arg);

            if (indexParam >= 0) {
                // Number of the determined piece (in addition to the initial configuration)
                if (++i >= args.length) { help(1); }
                numberPieces[indexParam] = Integer.parseInt(args[i]);
            }
            else {                  // other options
                switch (arg) {
                case "-all":
                    isAll = true;

                    break;

                case "-d":  // -d: Domination problem
                    isIndependenceProblem = false;

                    break;

                case "-debug-pos":
                    isDebugPos = true;

                    break;

                case "-load":  // -load - or -load FILENAME: initial configuration
                    if (++i >= args.length) { help(1); }

                    initialChessboard = new Chessboard(("-".equals(args[i])
                                                        ? null
                                                        : args[i]), isMuseumDisplay);
                    m = initialChessboard.getM();
                    n = initialChessboard.getN();

                    break;

                case "-help":
                    help(0);

                    break;

                case "-i":  // -i: Independence problem
                    isIndependenceProblem = true;

                    break;

                case "-latex":
                    isLatex = true;

                    break;

                case "-m":  // -m n: number of lines
                    if (++i >= args.length) { help(1); }
                    m = Integer.parseInt(args[i]);

                    if (m <= 0) { help(1); }
                    initialChessboard = null;

                    break;

                case "-museum":  // museum format to display solution
                    isMuseumDisplay = true;

                    break;

                case "-minmax":  // -z: minimize/maximize
                    isMinMax = true;

                    break;

                case "-n":  // -n n: number of columns
                    if (++i >= args.length) { help(1); }
                    n = Integer.parseInt(args[i]);

                    if (n <= 0) { help(1); }
                    initialChessboard = null;

                    break;

                case "-symmetrics-optimizations":
                    isSymmetricsOptimizations = true;

                    break;

                case "-verbose":
                    isVerbose = true;

                    break;

                default:
                    System.err.println("Unknow parameter \"" + arg + "\"");

                    help(1);
                }
            }
        }

        if (m == -1) {  // if m not specified
            m = n;
        }

        if ((m < 1) || (n < 1)) {
            help(1);
        }

        if (initialChessboard == null) {
            initialChessboard = new Chessboard(m, n);
        }

        if (isMinMax) {
            isAll = false;
        }

        // Set initialNumberPieces
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                ++initialNumberPieces[initialChessboard.get(i, j)];
            }
        }

        // Set numberPieces[0]
        numberPieces[0] = m*n - initialNumberPieces[0];
        for (int num = 1; num < numberPieces.length; ++num) {
            numberPieces[0] -= initialNumberPieces[num] + numberPieces[num];
        }

        if ((m != n) || (initialNumberPieces[EnumPiece.EMPTY.ordinal()] != m*n)) {
            // Disable isSymmetricsOptimizations
            isSymmetricsOptimizations = false;
        }
    }



    public static void main(String[] args) {
        final ChocoChess prog = new ChocoChess();

        try {
            prog.setParams(args);
        }
        catch (NumberFormatException e) {
            prog.help(1);
        }

        if (prog.isVerbose) {
            prog.printInfos();
        }

        prog.modelAndSolve();
    }

}
