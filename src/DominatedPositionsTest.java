/**
 * DominatedPositionsTest.java (May 25, 2017)
 *
 * GPLv3 --- Copyright (C) 2017
 *         - Dany Simone Efila,
 *         - Olivier Pirson --- http://www.opimedia.be/
 *
 * @author Dany Simone Efila
 * @author Olivier Pirson
 */

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;


public class DominatedPositionsTest {

    @Test
    public void empty1Test() {
        DominatedPositions pos = new DominatedPositions(1, 1, 0, 0);

        System.out.println("empty1");
        System.out.println(ChessboardTest.empty1);

        assertTrue(pos.isIndependent(ChessboardTest.empty1));
    }

}
