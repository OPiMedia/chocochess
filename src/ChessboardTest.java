/**
 * ChessboardTest.java (May 25, 2017)
 *
 * GPLv3 --- Copyright (C) 2017
 *         - Dany Simone Efila,
 *         - Olivier Pirson --- http://www.opimedia.be/
 *
 * @author Dany Simone Efila
 * @author Olivier Pirson
 */

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;


public class ChessboardTest {
    static int[][] boxesEmpty1 = {{0}};

    static int[][] boxesEmpty2 = {{0, 0},
                                  {0, 0}};

    static int[][] boxesEmpty3 = {{0, 0, 0},
                                  {0, 0, 0},
                                  {0, 0, 0}};


    static int[][] boxesIndependence3 = {{2, 0, 0},
                                         {3, 0, 0},
                                         {0, 1, 0}};

    static int[][] boxesIndependence4 = {{1, 0, 0, 0},
                                         {0, 1, 0, 0},
                                         {0, 0, 3, 0},
                                         {0, 0, 2, 3}};


    static int[][] boxesDomination3 = {{1, 0, 2},
                                       {0, 1, 0},
                                       {0, 0, 3}};

    static Chessboard empty1 = new Chessboard(boxesEmpty1);

    static Chessboard empty2 = new Chessboard(boxesEmpty2);

    static Chessboard empty3 = new Chessboard(boxesEmpty3);


    static Chessboard independence3 = new Chessboard(boxesIndependence3);

    static Chessboard independence4 = new Chessboard(boxesIndependence4);


    static Chessboard domination3 = new Chessboard(boxesDomination3);



    @Test
    public void empty1Test() {
        System.out.println("empty1");
        System.out.println(empty1);

        assertTrue(empty1.isIndependent());
        assertFalse(empty1.isDominated());
    }


    @Test
    public void empty2Test() {
        System.out.println("empty2");
        System.out.println(empty2);

        assertTrue(empty2.isIndependent());
        assertFalse(empty2.isDominated());
    }


    @Test
    public void empty3Test() {
        System.out.println("empty3");
        System.out.println(empty3);

        assertTrue(empty3.isIndependent());
        assertFalse(empty3.isDominated());
    }



    @Test
    public void independence3Test() {
        System.out.println("independence3");
        System.out.println(independence3);

        assertTrue(independence3.isIndependent());
        assertFalse(independence3.isDominated());
    }


    @Test
    public void independence4Test() {
        System.out.println("independence4");
        System.out.println(independence4);

        assertTrue(independence4.isIndependent());
        assertTrue(independence4.isDominated());
    }



    @Test
    public void domination3Test() {
        System.out.println("domination3");
        System.out.println(domination3);

        assertFalse(domination3.isIndependent());
        assertTrue(domination3.isDominated());
    }

}
