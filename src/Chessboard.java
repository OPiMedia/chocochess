/**
 * Chessboard.java (May 25, 2017)
 *
 * GPLv3 --- Copyright (C) 2017
 *         - Dany Simone Efila,
 *         - Olivier Pirson --- http://www.opimedia.be/
 *
 * @author Dany Simone Efila
 * @author Olivier Pirson
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;

import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;

import org.chocosolver.solver.Solution;
import org.chocosolver.solver.variables.IntVar;



/**
 * Empty box, each piece,
 * and (for DominatedPosition) position and position dominated
 */
enum EnumPiece {
    EMPTY,

    ROOK,    // rook (Tour)
    BISHOP,  // bishop (Fou)
    KNIGHT,  // knight (Cavalier)

    QUEEN,  // queen (Reine)
    KING,   // King (roi)

    OBSTACLE,  // "piece" without dominated position

    NORTHROOK,  // North-"rook" ("Tour" Nord)
    SOUTHROOK,  // South-"rook" ("Tour" Sud)
    EASTROOK,   // East-"rook" ("Tour" Est)
    WESTROOK,   // West-"rook" ("Tour" Ouest)

    DOMINATED,  // dominated position for a determinated piece at a determinated position

    MUSTBEEMPTY;  // to display position in -debug-pos that must be empty


    public String toString(boolean isMuseumDisplay) {
        return String.valueOf(Chessboard.toSymbol(ordinal(), isMuseumDisplay));
    }
}



/**
 * Chessboard of size m x n
 */
public class Chessboard {
    static final char[] SQUARE_SYMBOL
        = {'*',                 // symbol to display empty square
           'T', 'F', 'C',       // chess pieces
           'R', 'K',            // additional chess pieces
           '#',                 // obstacle
           'N', 'S', 'E', 'O',  // additional unidirectional-"rook"
           '@',                 // dominated position (for DominatedPosition)
           '.'};                // to display position in -debug-pos that must be empty

    static final char[] SQUARE_MUSEUM_SYMBOL
        = {' ',                 // symbol to display empty square
           'T', 'F', 'C',       // chess pieces
           'R', 'K',            // additional chess pieces
           '*',                 // obstacle
           'N', 'S', 'E', 'O',  // additional unidirectional-"rook"
           '@',                 // dominated position (for DominatedPosition)
           '.'};                // to display position in -debug-pos that must be empty



    static char toSymbol(int value, boolean isMuseumDisplay) {
        assert(SQUARE_SYMBOL.length == ChocoChess.NB_TYPE_PIECE + 1 + 2);
        assert(SQUARE_MUSEUM_SYMBOL.length == ChocoChess.NB_TYPE_PIECE + 1 + 2);

        assert(0 <= value);
        assert(value < SQUARE_SYMBOL.length);

        return (isMuseumDisplay
                ? SQUARE_MUSEUM_SYMBOL[value]
                : SQUARE_SYMBOL[value]);
    }



    final int m;
    final int n;
    final int[][] boxes;



    public Chessboard(int m, int n) {
        assert(m > 0);
        assert(n > 0);

        this.m = m;
        this.n = n;

        boxes = new int[m][n];
    }


    public Chessboard(int[][] boxes) {
        assert(boxes != null);
        assert(boxes.length != 0);

        m = boxes.length;
        n = boxes[0].length;

        this.boxes = new int[m][n];

        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                this.boxes[i][j] = boxes[i][j];
            }
        }
    }


    public Chessboard(IntVar[][] varBoxes, Solution solution) {
        assert(varBoxes != null);
        assert(varBoxes.length != 0);

        assert(solution != null);

        this.m = varBoxes.length;
        this.n = varBoxes[0].length;

        boxes = new int[m][n];

        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                boxes[i][j] = solution.getIntVal(varBoxes[i][j]);
            }
        }
    }


    /**
     * Load chessboard/museum in text file chess or museum format
     * (Chessboard.SQUARE_SYMBOL or Chessboard.SQUARE_MUSEUM_SYMBOL).
     */
    public Chessboard(String filename, boolean isMuseumDisplay) {
        // Read the file
        final ArrayList<String> lines = new ArrayList<String>();

        try {
            final BufferedReader buffer = new BufferedReader(filename == null
                                                             ? new InputStreamReader(System.in)
                                                             : new FileReader(new File(filename)));

            if (filename == null) {
                filename = "[stdin]";
            }

            boolean ok = true;
            String line;

            while ((line = buffer.readLine()) != null) {
                if (line.length() == 0) {
                    break;
                }
                if (line.length() % 2 == 0) {
                    ok = false;

                    break;
                }

                lines.add(line);
            }

            if (!ok) {
                System.err.println("Bad format error in \"" + filename + "\"!");

                System.exit(1);
            }
        }
        catch (FileNotFoundException e) {
            System.err.println("Filename \"" + filename + "\" NOT opened!");

            System.exit(1);
        }
        catch (IOException e) {
            System.err.println("Reading error of \"" + filename + "\"!");

            System.exit(1);
        }


        // Prepare inverted map of characters
        final HashMap<Character, Integer> chars = new HashMap<Character, Integer>();

        assert(SQUARE_SYMBOL.length == SQUARE_MUSEUM_SYMBOL.length);

        for (int i = 0; i < SQUARE_SYMBOL.length; ++i) {
            final char c = toSymbol(i, isMuseumDisplay);

            chars.put(c, i);
        }


        // Parse the strings and build the chessboard
        int lineError = -1;
        char charError = 0;

        m = lines.size();
        if (m == 0) {
            n = 0;
            boxes = null;
            lineError = 0;
        }
        else {
            n = (((String)lines.get(0)).length() + 1)/2;

            boxes = new int[m][n];

            for (int i = 0; i < m; ++i) {
                for (int j = 0; j < n; ++j) {
                    boxes[i][j] = EnumPiece.OBSTACLE.ordinal();
                }
            }

            int i = 0;
            for (String line : lines) {
                if (line.length() > 2*n - 1) {
                    if (lineError < 0) {
                        lineError = i;
                    }

                    break;
                }

                for (int j = 0; j < line.length(); j += 2) {
                    final char symbol = line.charAt(j);

                    if (!chars.containsKey(symbol)) {
                        if (lineError < 0) {
                            lineError = i;
                            charError = symbol;
                        }

                        break;
                    }

                    if ((j + 1)/2 < n) {

                        boxes[i][(j + 1)/2] = chars.get(symbol);
                    }
                    else {
                        if (lineError < 0) {
                            lineError = i;
                            charError = symbol;
                        }

                        break;
                    }
                }

                ++i;
            }
        }

        if (lineError >= 0) {
            System.err.println("Bad format error in line " + lineError
                               + " of \"" + filename + "\": \"" + charError + "\"!");
            System.err.println(this.toString(isMuseumDisplay));

            System.exit(1);
        }
    }


    public int get(int i, int j) {
        assert(0 <= i);
        assert(i < m);

        assert(0 <= j);
        assert(j < n);

        return boxes[i][j];
    }


    public int getM() { return m; }

    public int getN() { return n; }


    /**
     * Return true iff all boxes of the chessboard are occupied or dominated.
     */
    public boolean isDominated() {
        assert(EnumPiece.EMPTY.ordinal() == 0);

        final boolean[][] dominateds = new boolean[m][n];
        int nb_dominated = 0;

        // For each box
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                assert(0 <= boxes[i][j]);
                assert(boxes[i][j] <= ChocoChess.NB_TYPE_PIECE);

                if (boxes[i][j] == EnumPiece.EMPTY.ordinal()) {
                    // Check there exists piece at position (posI, posJ)
                    // that dominates this empty box (i, j)
                    for (int posI = 0; posI < m; ++posI) {
                        for (int posJ = 0; posJ < n; ++posJ) {
                            final DominatedPositions dominatedPositions
                                = new DominatedPositions(m, n, posI, posJ);

                            if (boxes[posI][posJ] != EnumPiece.EMPTY.ordinal()) {
                                dominatedPositions.add(boxes[posI][posJ]);

                                if (!dominateds[i][j]
                                    && dominatedPositions.isDominated(this, i, j)) {
                                    // Piece at (posI, posJ) dominates box (i, j) not already marked
                                    dominateds[i][j] = true;
                                    ++nb_dominated;
                                }
                            }
                        }
                    }
                }
                else if (!dominateds[i][j]) {  // occupied box not already marked
                    dominateds[i][j] = true;
                    ++nb_dominated;
                }
            }
        }

        return nb_dominated == m*n;
    }


    /**
     * Return true iff all pieces of the chessboard are independent.
     */
    public boolean isIndependent() {
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                assert(0 <= boxes[i][j]);
                assert(boxes[i][j] <= ChocoChess.NB_TYPE_PIECE);

                if (boxes[i][j] != EnumPiece.EMPTY.ordinal()) {  // check this piece
                    final DominatedPositions dominatedPositions = new DominatedPositions(m, n,
                                                                                         i, j);

                    dominatedPositions.add(boxes[i][j]);

                    if (!dominatedPositions.isIndependent(this)) {
                        return false;
                    }
                }
            }
        }

        return true;
    }


    public void printAllDominatedPositions(boolean isMuseumDisplay) {
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                final int num = get(i, j);

                if (num != EnumPiece.EMPTY.ordinal()) {
                    final DominatedPositions dominatedPositions
                        = new DominatedPositions(m, n, i, j);

                    System.err.println();
                    System.err.println(i + ", " + j + ": "
                                       + Chessboard.toSymbol(num, isMuseumDisplay));
                    System.err.println("=======");

                    dominatedPositions.add(num);

                    System.err.println(dominatedPositions.toString(toSymbol(num, isMuseumDisplay),
                                                                   isMuseumDisplay));

                    dominatedPositions.printAllPositionsMustBeEmpty(boxes[i][j],
                                                                    isMuseumDisplay);
                }
            }
        }
    }


    @Override
    public String toString() {
        return toString(false);
    }


    public String toString(boolean isMuseumDisplay) {
        final String[] lines = new String[m];

        for (int i = 0; i < m; ++i) {
            final String line[] = new String[n];

            for (int j = 0; j < n; ++j) {
                line[j] = String.valueOf(toSymbol(boxes[i][j], isMuseumDisplay));
            }

            lines[i] = String.join(" ", line);
        }

        return String.join("\n", lines);
    }


    /**
     * Build a string LaTeX representation of the chessboard
     * for the chessboard package:
     * https://www.ctan.org/pkg/chessboard
     */
    public String toStringLatex() {
        final char[] LATEX
            = {' ',                 // symbol to display empty square
               'R', 'B', 'N',       // chess pieces
               'Q', 'K',            // additional chess pieces
               ' ',                 // obstacle
               ' ', ' ', ' ', ' ',  // additional unidirectional-"rook"
               ' ',                 // dominated position (for DominatedPosition)
               ' '};                // to display position in -debug-pos that must be empty

        final ArrayList<String> pieces = new ArrayList<String>();
        final ArrayList<String> specials = new ArrayList<String>();

        for (int i = 0; i < m; ++i) {
            final int figure = m - i;

            for (int j = 0; j < n; ++j) {
                final char letter = (char)('a' + j);

                if (boxes[i][j] != EnumPiece.EMPTY.ordinal()) {
                    final char symbol = LATEX[boxes[i][j]];

                    if (symbol != ' ') {
                        pieces.add(String.valueOf(symbol) + letter + figure);
                    }
                    else if (boxes[i][j] == EnumPiece.OBSTACLE.ordinal()) {
                        specials.add("pgfstyle=color,markfields={" + letter + figure + '}');
                    }
                    else if (boxes[i][j] == EnumPiece.NORTHROOK.ordinal()) {
                        specials.add("pgfstyle=straightmove,markmoves={"
                                     + letter + figure + '-' + letter + (figure + 1) + '}');
                    }
                    else if (boxes[i][j] == EnumPiece.SOUTHROOK.ordinal()) {
                        specials.add("pgfstyle=straightmove,markmoves={"
                                     + letter + figure + '-' + letter + (figure - 1) + '}');
                    }
                    else if (boxes[i][j] == EnumPiece.EASTROOK.ordinal()) {
                        specials.add("pgfstyle=straightmove,markmoves={"
                                     + letter + figure + '-' + (char)(letter + 1) + figure + '}');
                    }
                    else if (boxes[i][j] == EnumPiece.WESTROOK.ordinal()) {
                        specials.add("pgfstyle=straightmove,markmoves={"
                                     + letter + figure + '-' + (char)(letter - 1) + figure + '}');
                    }
                }
            }
        }

        final String setblack
            = (pieces.size() == 0
               ? ""
               : ",\nsetblack={" + String.join(",", pieces.toArray(new String[pieces.size()])) + '}');

        final String special
            = (specials.size() == 0
               ? ""
               : ",\n" + String.join(",", specials.toArray(new String[specials.size()])));

        return "\\chessboard[label=false,padding=-1pt,pgf=true,showmover=false,maxfield="
            + String.valueOf((char)('a' + (n - 1))) + m
            + setblack + special + "]";
    }
}
