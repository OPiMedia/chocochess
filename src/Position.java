/**
 * Position.java (May 25, 2017)
 *
 * GPLv3 --- Copyright (C) 2017
 *         - Dany Simone Efila,
 *         - Olivier Pirson --- http://www.opimedia.be/
 *
 * @author Dany Simone Efila
 * @author Olivier Pirson
 */


/**
 * Position i, j.
 */
public class Position {
    final int posI;
    final int posJ;


    public Position(int posI, int posJ) {
        assert(0 <= posI);
        assert(0 <= posJ);

        this.posI = posI;
        this.posJ = posJ;
    }


    public int getPosI() { return posI; }

    public int getPosJ() { return posJ; }


    @Override
    public String toString() {
        return "(" + getPosI() + ", " + getPosJ() + ')';
    }
}
